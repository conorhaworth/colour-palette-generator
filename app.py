from flask import Flask, render_template, request, redirect
from sklearn.cluster import KMeans
from collections import Counter
from PIL import Image, UnidentifiedImageError
import webcolors
from io import BytesIO
import numpy as np
app = Flask(__name__)

TEMPLATE = "palette.html"

@app.route("/", methods=["GET", "POST"])
def renderColourPalette():
    colours = []
    if request.method == "POST":
        if request.files:
            try:
                image = request.files["image"]
                num_of_colours = int(request.form["number_of_colours"])
                return render_template(TEMPLATE, colours=get_most_dominant_colours(image, num_of_colours))
            except UnidentifiedImageError as e:
                errors = "Error: Invalid image"
                return render_template(TEMPLATE, colours=colours, errors=errors)
            except ValueError as e:
                errors = "Error: Invalid number of colours"
                return render_template(TEMPLATE, colours=colours, errors=errors)

    return render_template(TEMPLATE, colours=colours)

def get_most_dominant_colours(image, num_of_colours): 
    im = Image.open(BytesIO(image.read()))
    im = im.resize((150, 150))
    pixels = im.load()
    width = im.size[0];
    height = im.size[1];

    cluster = KMeans(n_clusters = num_of_colours)
    labels = cluster.fit_predict(im.getdata())
    label_counts = Counter(labels)
    most_dominant_colours = list()
    for label in label_counts.most_common(num_of_colours):
        dominant_colour = cluster.cluster_centers_[label[0]]
        dominant_colour = tuple(np.floor(dominant_colour).astype(np.int))
        most_dominant_colours.append(webcolors.rgb_to_hex(dominant_colour))

    return most_dominant_colours


if __name__ == "__main__":
    app.run()