# Colour palette generator
Simple flask app to get most common colours from an image to build a colour palette.

### Live demo
http://conorhaworth.pythonanywhere.com/

## Setup
Install dependencies:
`python setup.py`

Run server:
`flask run`
